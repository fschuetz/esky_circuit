# esky_circuit

A simple replacement part to fix an esky (portable cooler). Drives a motor to spin the cooling fan and a peltier module.

## Application
This circuit was developed as a replacement an existing circuit in an esky that was old and
the pcb was obviously so well manufactured that it literally broke due to brittleness. However,
why throw away a perfectly functioning cooling box if you can just replace the electronics.

Note: You may want to adapt the board form factor and or parts to fit your specific cooler.

## Bill of Materials
| Name | Value | Source |
| ---- | ----- | ------ |
| Permanent Magnet Motor | 6V, JGB37-500-12560 | [Aliexpress](https://www.aliexpress.com/item/4000090722774.html) |
| Peltier Module | Tec1-12704 | [Aliexpress](https://www.aliexpress.com/item/4001297694030.html) |
| Diode | 1N4001 | [Aliexpress](https://www.aliexpress.com/item/1005002377373818.html) |
| Diode | 6A05 | [Aliexpress](https://www.aliexpress.com/item/1005001620935527.html) |
| Carbon Film Resistor | 24R | [Aliexpress](https://www.aliexpress.com/item/1005002258658274.html) |

## Workring of the Electronics
This specific type of esky works in a very simple way. A peltier module is sandwiched between
two blocks of aluminium with cooling fins. It works as a recuperator between the aluminium 
block that comes from the inside of the esky and the aluminium block that sits on the outside
of the esky. As the peltier module produces heat on the outside to generate the chilling 
effect inside the esky, the heat forming on the outside aluminimum block must be carried away.

This is achieved by having a fan sucking in fresh air and blowing it through the cooling fins
of the alluminium block to the outside. This effectively carries the heat away. 

```
IMPORTANT: Always make sure you have the motor poles connected such that it drives correctly. 
When it spins in the wrong direction the hot air might not be carried away properly.
```

The circuit in itself is very simple. There are two diodes that providing reverse polarity proteciton.
In the power path of the motor there is an additonal resistor that ensure the motor is 
driven within specification.

![Esky](./images/box_full.jpg)
![Inside](./images/box_inside.jpg)
![Airflow View](./images/box_outside_airflow.jpg)
![Wiring](./images/box_outside_wiring.jpg)
![Electronics](./images/box_outside_electronics.jpg)
![Populated PCB](./images/assembled_pcb.png)
